#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "funkcje.h"



int main()
{
    FILE *plik ;
    if((plik=fopen("P0001_attr.txt","a+"))==NULL)
    {
         printf("blad otwarcia pliku\n");
    }

    /*  Liczba wierszy N */ 
    int N = 55; 
    /*  Liczba kolumn M  */ 
    int M = 3; 

    double **t; 
    t = malloc( N * sizeof( double * ) ); 
    for (int i = 0; i < N; i++ ) 
    { 
        t[i] = malloc( M * sizeof( double ) ); 
    } 
    
    for ( int i = 0; i < N; i++ ) 
    { 
        for (int  j = 0; j < M; j++ ) 
            t[i][j]=0; 
    } 
   /* for (int i = 0; i < N; i++ ) 
    { 
        for (int  j = 0; j < M; j++ ) 
            printf("%.3f  ", t[i][j]); 
        printf("\n"); 
    } 
    */

   char znak="";
   char liczba[10]="";
   int n=0;
   int kolumna=0,wiersz=0;

   while(1)
    {
        znak = fgetc(plik); //zapisuję jeden znak z pliku
        if(znak==9 || znak==EOF)
        {
            double liczba1;
                for (int i=0;i<strlen(liczba);i++)
                {
                    if (!isdigit(liczba[i])) 
                    {
                        break;
                    }
                }
            liczba1=atof(liczba);

            if(liczba1!=0)
            {
                if(kolumna==3)
                {
                    wiersz+=1;
                    kolumna=0;
                }
                t[wiersz][kolumna]=liczba1;
                kolumna++;
            }
            for(int i=0;i<10;i++)
                liczba[i]='\0';
            n=0;
         if(znak==EOF)
        {
            break;
        }
        }else
        {
            if(znak!=32)
            {
                liczba[n]=znak;
                n++;
            }   
        }   
    }

    double sre[3];
    for(int i=0;i<3;i++)
        sre[i]=srednia(t, i);

    for(int i=0;i<3;i++)
        sortowanie(t, i);
    
    double med[3];
    for(int i=0;i<3;i++)
        med[i]=mediana(t, i);

    double odch[3];
    for(int i=0;i<3;i++)
        odch[i]=odchylenie(t, i, sre);
   
    fprintf(plik,"\n51.    %.5f    %.5f    %.3f", sre[0],sre[1],sre[2]);
    fprintf(plik,"\n52.    %.5f    %.5f    %.3f", med[0],med[1],med[2]);
    fprintf(plik,"\n53.    %.5f    %.5f    %.3f", odch[0],odch[1],odch[2]);
    for ( int k=0; k<N; k++)
        free ( t[k] ) ;

    fclose(plik);

    return 0;
}
