CC=gcc
CFLAGS=-Wall
LIBS=-lm

Zad1: Zad1.o srednia.o
	$(CC) $(CFLAGS) -o Zad1 Zad1.o odchylenie.o mediana.o sortowanie.o srednia.o $(LIBS)
 
Zad1.o: zad1.c
	$(CC) $(CFLAGS) -c zad1.c

odchylenie.o: odchylenie.c
	$(CC) $(CFLAGS) -c odchylenie.c

mediana.o: mediana.c
	$(CC) $(CFLAGS) -c mediana.c

sortowanie.o: sortowanie.c
	$(CC) $(CFLAGS) -c sortowanie.c	

srednia.o: srednia.c
	$(CC) $(CFLAGS) -c srednia.c