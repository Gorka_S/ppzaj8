#include "funkcje.h"

void sortowanie(double **p_t,int p_i)
{
     int j=0;
     double pom=0;
     for(int i=1; i<50; i++)
     {
             //wstawienie elementu w odpowiednie miejsce
             pom = p_t[i][p_i]; //ten element będzie wstawiony w odpowiednie miejsce
             j = i-1;
             
             //przesuwanie elementów większych od pom
             while(j>=0 && p_t[j][p_i]>pom) 
             {
                    p_t[j+1][p_i] = p_t[j][p_i]; //przesuwanie elementów
                    --j;
             }
             p_t[j+1][p_i] = pom; //wstawienie pom w odpowiednie miejsce
     }  
    return;   
}