#include "funkcje.h"

double odchylenie(double **p_t,int p_i, double p_sre[])
{
    int i=0;
    double kwadrat=0;
    for(int i=0;i<50;i++)
        kwadrat+=pow(p_t[i][p_i]-p_sre[p_i],2);
    kwadrat/=50;
    kwadrat=sqrt(kwadrat);
    return kwadrat;
}